package com.shemy.plantsvszombies;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.opengl.CCTexture2D;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.util.CGPointUtil;

/**
 * Created by Dzsom on 2018/11/27.
 */

public abstract class Bullet extends CCSprite {

    private int speed = 150;
    private int attack=20;
    private ShootPlant shootPlant;

    public Bullet(String filepath, ShootPlant shootPlant) {
        super(filepath);
        this.shootPlant = shootPlant;
        setPosition(shootPlant.getPosition().x+20,shootPlant.getPosition().y+50);
        shootPlant.getParent().addChild(this,6);
        shootPlant.getBullets().add(this);
        move();
    }

    private void move() {

        CGPoint end = ccp(1400,getPosition().y);
        float t = CGPointUtil.distance(getPosition(),end)/speed;
        CCMoveTo ccMoveTo = CCMoveTo.action(t,end);
        CCCallFunc ccCallFunc =CCCallFunc.action(this,"end");
        CCSequence ccSequence = CCSequence.actions(ccMoveTo,ccCallFunc);
        runAction(ccSequence);
    }

    public void end(){
        removeSelf();
        shootPlant.getBullets().remove(this);
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public abstract void showBulletBlast(Zombie zombie);

    public abstract void showBulletBlast(ZombieImp zombieImp);


}
